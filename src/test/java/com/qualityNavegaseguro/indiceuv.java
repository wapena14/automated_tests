package com.qualityNavegaseguro;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class indiceuv {
	private WebDriver driver;
	By ingresouv =  By.xpath("/html/body/app-root/ion-app/ion-router-outlet/app-smart-analysis-detail/ion-content/ion-segment/ion-segment-button[4]");
	
	
	public void ingresoiuv(WebDriver driver) {
	WebDriverWait wait = new WebDriverWait(driver,60);
	wait.until(ExpectedConditions.visibilityOfElementLocated(ingresouv));
	driver.findElement(ingresouv).click();
	}
	
	
}

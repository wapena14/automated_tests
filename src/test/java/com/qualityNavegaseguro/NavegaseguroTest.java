package com.qualityNavegaseguro;

import static org.junit.Assert.*;

import java.awt.Dimension;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

public class NavegaseguroTest {

	private WebDriver driver;
	By registerPageLocater = By.xpath("//img[@src='assets/img/logo.png']");
	By correoLocator = By.name("ion-input-0");
	By contraseñaLocator = By.name("ion-input-1");
	By ingresoLocator = By.xpath("//ion-button");
	By ingresoEstacion =  By.xpath("/html/body/app-root/ion-app/ion-router-outlet/app-menu/ion-tabs/div/ion-router-outlet/app-location/ion-content/ion-card[1]/ion-card-title");
	
	@Before
	
	public void setUp() throws Exception {
		ChromeOptions options = new ChromeOptions();
		options.addArguments("--user-agent=iPhone SE");
		System.setProperty("webdriver.chrome.driver","./src/test/resources/chromedriver/chromedriver.exe");
		driver = new ChromeDriver(options);
		org.openqa.selenium.Dimension d = new org.openqa.selenium.Dimension(300, 750);
		driver.manage().window().setSize(d);
		driver.get("http://localhost:4200/login");
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}

	@Test
	public void login() throws InterruptedException {
		Thread.sleep(6000);
		if(driver.findElement(registerPageLocater).isDisplayed()) {
			driver.findElement(correoLocator).sendKeys("Linalmg@hotmail.com");
			driver.findElement(contraseñaLocator).sendKeys("Prueba123*");
			driver.findElement(ingresoLocator).click();
		}else {
			System.out.print("Register pages was not found");
		}
		
		home h1=new home();
		h1.opcioncartagena(driver);
		
		Thread.sleep(3000);
		JavascriptExecutor js= (JavascriptExecutor)driver;
		
		WebElement flag=driver.findElement(By.xpath("/html/body/app-root/ion-app/ion-router-outlet/app-location-information/ion-content/div[1]/div[6]/ion-item[2]/ion-label"));
		js.executeScript("arguments[0].scrollIntoView();", flag);
		
		analisisinteligente i1 = new analisisinteligente();
		i1.ingresointeligentedi(driver);
		
		navegates n1 = new navegates();
		n1.ingresonavegates(driver);
		
		indiceuv uv1 = new indiceuv();
		uv1.ingresoiuv(driver);
		Thread.sleep(6000);
		
		WebElement flag11=driver.findElement(By.xpath("/html/body/app-root/ion-app/ion-router-outlet/app-smart-analysis-detail/ion-content/div[2]/ion-card/ion-grid/ion-row/ion-col[2]/div"));
		js.executeScript("arguments[0].scrollIntoView();", flag11);
		Thread.sleep(6000);
		
		WebElement flag2=driver.findElement(By.xpath("/html/body/app-root/ion-app/ion-router-outlet/app-smart-analysis-detail/ion-content/div[2]/ion-card/div[2]/ion-card-header/ion-card-title"));
		js.executeScript("arguments[0].scrollIntoView();", flag2);
		Thread.sleep(6000);
		
		
		WebElement flag7=driver.findElement(By.xpath("/html/body/app-root/ion-app/ion-router-outlet/app-smart-analysis-detail/ion-content/div[2]/ion-card/div[2]/ion-card-header[2]/ion-card-title"));
		js.executeScript("arguments[0].scrollIntoView();", flag7);
		Thread.sleep(6000);
		
		
		regresarnavegacion in1 = new regresarnavegacion();
		in1.regresarnavega(driver);
		
		regresarestacion re1 = new regresarestacion();
		re1.regresaestacion(driver);
		
		ingresoaotraestacion ie1 = new ingresoaotraestacion();
		ie1.ingresootraestacion(driver);

		Thread.sleep(6000);
		JavascriptExecutor js3= (JavascriptExecutor)driver;
		
		WebElement flag3=driver.findElement(By.xpath("/html/body/app-root/ion-app/ion-router-outlet/app-location-information/ion-content/div[1]/div[6]/ion-item[2]/ion-label"));
		js3.executeScript("arguments[0].scrollIntoView();", flag3);
		
		Thread.sleep(4000);
		
		analisisinteligente i2 = new analisisinteligente();
		i2.ingresointeligentedi(driver);
		
		navegates n2 = new navegates();
		n2.ingresonavegates(driver);
		
		indiceuv uv2 = new indiceuv();
		uv2.ingresoiuv(driver);
		Thread.sleep(6000);
		
		WebElement flag12=driver.findElement(By.xpath("/html/body/app-root/ion-app/ion-router-outlet/app-smart-analysis-detail/ion-content/div[2]/ion-card/ion-grid/ion-row/ion-col[2]/div"));
		js.executeScript("arguments[0].scrollIntoView();", flag12);
		Thread.sleep(6000);
		
		
		WebElement flag4=driver.findElement(By.xpath("/html/body/app-root/ion-app/ion-router-outlet/app-smart-analysis-detail/ion-content/div[2]/ion-card/div[2]/ion-card-header/ion-card-title"));
		js.executeScript("arguments[0].scrollIntoView();", flag4);
		Thread.sleep(6000);
		
		WebElement flag9=driver.findElement(By.xpath("/html/body/app-root/ion-app/ion-router-outlet/app-smart-analysis-detail/ion-content/div[2]/ion-card/div[2]/ion-card-header[2]/ion-card-title"));
		js.executeScript("arguments[0].scrollIntoView();", flag9);
		Thread.sleep(6000);
		
	}
	
}

package com.qualityNavegaseguro;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class regresarnavegacion {
	private WebDriver driver;
	By regresarnavega =  By.xpath("/html/body/app-root/ion-app/ion-router-outlet/app-smart-analysis-detail/ion-header/ion-toolbar/ion-icon");
	
	
	public void regresarnavega(WebDriver driver) {
	WebDriverWait wait = new WebDriverWait(driver,60);
	wait.until(ExpectedConditions.visibilityOfElementLocated(regresarnavega));
	driver.findElement(regresarnavega).click();
	}
}

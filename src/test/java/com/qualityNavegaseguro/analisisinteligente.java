package com.qualityNavegaseguro;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class analisisinteligente {
	
	
	private WebDriver driver;
	By ingresointeligente =  By.xpath("/html/body/app-root/ion-app/ion-router-outlet/app-location-information/ion-content/div[2]/ion-button");
	
	
	public void ingresointeligentedi(WebDriver driver) {
	WebDriverWait wait = new WebDriverWait(driver,60);
	wait.until(ExpectedConditions.visibilityOfElementLocated(ingresointeligente));
	driver.findElement(ingresointeligente).click();
	
	}
}

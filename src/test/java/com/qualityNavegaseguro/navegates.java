package com.qualityNavegaseguro;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class navegates {
	private WebDriver driver;
	By ingresonavegates =  By.xpath("/html/body/app-root/ion-app/ion-router-outlet/app-smart-analysis/ion-content/div/div[2]/div[1]/p");
	
	
	public void ingresonavegates(WebDriver driver) {
	WebDriverWait wait = new WebDriverWait(driver,60);
	wait.until(ExpectedConditions.visibilityOfElementLocated(ingresonavegates));
	driver.findElement(ingresonavegates).click();
	}
}

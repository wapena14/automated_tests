package com.qualityNavegaseguro;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ingresoaotraestacion {
	private WebDriver driver;
	By ingresootraestacion =  By.xpath("/html/body/app-root/ion-app/ion-router-outlet/app-menu/ion-tabs/div/ion-router-outlet/app-location/ion-content/ion-card[2]/ion-card-title");
	
	
	public void ingresootraestacion(WebDriver driver) {
	WebDriverWait wait = new WebDriverWait(driver,60);
	wait.until(ExpectedConditions.visibilityOfElementLocated(ingresootraestacion));
	driver.findElement(ingresootraestacion).click();
	}
}






package com.qualityNavegaseguro;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class regresarestacion {
	private WebDriver driver;
	By regresaestacion =  By.xpath("/html/body/app-root/ion-app/ion-router-outlet/app-smart-analysis/ion-header/ion-toolbar/ion-icon");
	
	
	public void regresaestacion(WebDriver driver) {
	WebDriverWait wait = new WebDriverWait(driver,60);
	wait.until(ExpectedConditions.visibilityOfElementLocated(regresaestacion));
	driver.findElement(regresaestacion).click();
	}
}
